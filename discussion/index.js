//JavaScript Operartors

//Arithmetic Operators


let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Result of addition operators: " + sum);

let difference = x - y;
console.log("Result of substraction operator: " + difference);

let product = x * y;
console.log("Result of multiplication operator: " + product);

let quotient = x / y;
console.log("Result of division operator: " + quotient);

let remainder = y % x;
console.log("Result of modulo operator: " + remainder);

// Assignment Operators (=) - assigns the value of the right operand to a variable

let assignmentNumber = 8;

//Addition Assignment Operator (+)

assignmentNumber = assignmentNumber + 2;
console.log("Result of addition assignment operator: " + assignmentNumber);

//Shorthand
assignmentNumber +=2;
console.log("Result of addition assignment operator: " + assignmentNumber);

// Subtraction/Multiplication/Division Assignment Operator (-=, *=, /=);
assignmentNumber -=2;
console.log("Result of substraction assignment operator: " + assignmentNumber);
assignmentNumber *=2;
console.log("Result of multiplication assignment operator: " + assignmentNumber);
assignmentNumber /=2;
console.log("Result of division assignment operator: " + assignmentNumber);

// Multiple Operators and Parenthesis

let mdas  = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas);

// Using Parenthesis
let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas);

pemdas = (1 + (2 - 3)) * (4 / 5);
console.log ("Result of pemdas operation: " + pemdas);

// Increment and Decement (==/--)

let z = 1;

let increment = ++z;
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);

// post-increment
increment = z++;
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment: " + z);

let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

decrement = z--;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

// Type Coercion

let numA = '10';
let numB =  12

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion)

let numC = true + 1;
console.log(numC)

// comparison operators

let juan = 'juan';

// equality operator (==)

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1');
console.log(0 == false);
console.log('juan' == juan);

// Inequality operator (!=)
console.log(1 != 1);
console.log(1 != 2);
console.log(1 != '1');
console.log(0 != false);
console.log('juan' != juan);

// strict equality operator (===)
console.log(1 === 1);
console.log(1 === 2);
console.log(1 === '1');
console.log(0 === false);
console.log('juan' === juan);

// strict equality operator (!==)
console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== '1');
console.log(0 !== false);
console.log('juan' !== juan);

// Rational operators
let a = 50;
let b = 65;

// greater than (>)
let isGreaterThan = a > b;
// Less Than (<)
let isLessThan = a < b;
// Greater Than or Equal (>=);
let isGTorEqual = a >= b;
// Less Than or Equal
let isLTorEqual = a <= b;

console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTorEqual);
console.log(isLTorEqual);

// Logical And Operator 
let isLegalAge = true;
let isRegistered = false;

// Logical and Operator (&&)
// Returns true if all operands are true
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND Operator: " + allRequirementsMet);

// logical Or operator (||)
// returns true if one of the operands are true
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR operator:: " + someRequirementsMet);

// Logical Not Operator (!)
// returns the oposite value
let someRequirementsNotMet = !isRegistered;
console.log("Result of logical NOT operator: " + someRequirementsNotMet);

